var gulp = require('gulp'),
    rename = require('gulp-rename'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    livereload = require('gulp-livereload'),
    connect = require('gulp-connect'),
    cleanCSS = require('gulp-clean-css'),
    imagemin = require('gulp-imagemin'),
    pug = require('gulp-pug'),
    spritesmith = require('gulp.spritesmith'),
    concat = require('gulp-concat');

// server connect
gulp.task('connect', function() {
    connect.server({
        root: 'build',
        port: 9000,
        livereload: true
    });
});


//sass
gulp.task('sass', function () {
    return gulp.src('src/scss/style.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(concat('style.css'))
        .pipe(autoprefixer({
            browsers: ['last 6 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('build/css/'))
        .pipe(connect.reload());
});

//pug
gulp.task('pug', function buildHTML() {
    return gulp.src('src/*.pug')
        .pipe(pug())
        .pipe(gulp.dest('build/'))
        .pipe(connect.reload());

});

//minify
gulp.task('minify', function() {
    gulp.src('build/css/style.css')
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(rename('style.min.css'))
        .pipe(gulp.dest('build/css/'))
        .pipe(connect.reload());
});

//js
gulp.task('js', function () {
    gulp.src('src/js/**/*.js')
        .pipe(gulp.dest('build/js/'))
        .pipe(connect.reload());
});

//image
gulp.task('imagemin', function () {
    gulp.src('src/img/**/**/*')
        .pipe(imagemin())
        .pipe(gulp.dest('build/img'))
});

//fonts
gulp.task('fonts', function () {
    gulp.src('src/fonts/**/*')
        .pipe(gulp.dest('build/fonts'))
});

//sprite
gulp.task('sprite', function () {
    var spriteData = gulp.src('src/img/sprite/*.png').pipe(spritesmith({
        imgName: 'sprite.png',
        cssName: 'sprite.css'
    }));
    return spriteData.pipe(gulp.dest('build/img/sprites/'));
});

//watch
gulp.task('watch', function () {
    gulp.watch('src/scss/**/*.scss', ['sass']),
    gulp.watch('src/*.pug', ['pug']),
    gulp.watch('src/js/**/*.js', ['js']),
    gulp.watch('src/img/**/*', ['imagemin']);

});

//default
gulp.task('default', ['connect', 'sass', 'pug', 'js', 'minify', 'imagemin', 'fonts', 'watch']);
